set   :user,            "capistrano"
set   :domain,          "46.17.67.177:2222"
role  :app,             "46.17.67.177:2222", "46.17.67.177:2223"
set   :deploy_to,       "/var/www/doctipharma/src/comments"
set   :shared_files,    [app_path + "/config/parameters.yml", web_path + "/.htaccess"]
set   :branch,          "master"

server domain, :app, :web, :primary => true
