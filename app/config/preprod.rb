set   :user,            "capistrano"
set   :domain,          "46.17.67.191:2222"
role  :app,             "46.17.67.191:2222", "46.17.67.191:2223"
set   :deploy_to,       "/var/www/doctipharma/src/comments"
set   :branch,          "#{branch}"

server domain, :app, :web, :primary => true
