set   :user,            "root"
set   :domain,          "172.20.0.40"
role  :app, "172.20.0.40", "172.20.0.41"
set   :deploy_to,       "/var/www/doctipharma/src/comments"
set   :repository,      "."
set   :deploy_via,      "copy"
set   :scm,             "none"
set   :copy_dir,        "/home/jenkins/tmp/staging/comments"
set   :remote_copy_dir, "/tmp"
set   :composer_options, '--no-interaction --no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --quiet'

server domain, :app, :web, :primary => true
