set   :user,            "root"
set   :domain,          "172.20.0.47"
role  :app,             "172.20.0.47", "172.20.0.46"
set   :deploy_to,       "/var/www/doctipharma/src/comments"
set   :repository,      "."
set   :deploy_via,      "copy"
set   :scm,             "none"
set   :copy_dir,        "/home/jenkins/tmp/recette/comments"
set   :remote_copy_dir, "/tmp"
set   :composer_options, '--no-interaction --no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --quiet'
set   :php_bin,         "/usr/bin/php5"

server domain, :app, :web, :primary => true
