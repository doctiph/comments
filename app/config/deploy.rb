set   :stages,              %w(production preprod preprod-jenkins recette recette-jenkins)
set   :default_stage,       "preprod"
set   :stage_dir,            "app/config"
require 'capistrano/ext/multistage'

set   :application,         "Doctipharma Comments"

set   :deploy_via,          :rsync_with_remote_cache
set   :rsync_options,       '-rlgoDz --delete'

set   :scm,                 :git
set   :repository,          "https://doctiph@bitbucket.org/doctiph/comments.git"

set   :php_bin,             "/usr/bin/php54"
set   :use_composer,        true
set   :composer_options,    '--no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --quiet'
# Add --no-interaction to composer and avoid request pty to server
set   :interactive_mode,    false
set   :dump_assetic_assets, true
set   :clear_controllers,   false

set   :shared_files,        [app_path + "/config/parameters.yml"]
set   :shared_children,     [app_path + "/logs", 'data']

set   :writable_dirs,       [app_path + "/cache", app_path + "/logs", "data"]
set   :webserver_user,      "apache"
set   :permission_method,   :acl
set   :use_set_permissions, true
set   :use_sudo,            false
set   :keep_releases,       5

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL
