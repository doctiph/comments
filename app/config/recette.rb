set   :user,            "root"
set   :domain,          "172.20.0.47"
role  :app,             "172.20.0.47", "172.20.0.46"
set   :deploy_to,       "/var/www/doctipharma/src/comments"
set   :branch,          "#{branch}"
set   :php_bin,         "/usr/bin/php5"

server domain, :app, :web, :primary => true
