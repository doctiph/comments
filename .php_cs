<?php
$header = '';

return PhpCsFixer\Config::create()
    ->setRules(array(
        '@Symfony' => true,
        'header_comment' => array('header' => $header),

        //Don't remove align space
        'binary_operator_spaces' => false,
        //Force align space
        // 'binary_operator_spaces' => ['align_equals' => true, 'align_double_arrow' => true],

        'concat_space' => ['spacing' => 'one'],
        'array_syntax' => ['syntax' => 'short'],

        'ordered_class_elements' => true,
        'ordered_imports' => true,
        'phpdoc_add_missing_param_annotation' => true,
        'phpdoc_order' => true,

    ))
;
