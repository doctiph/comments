<?php

namespace Doctipharma\Component\Guzzle\Http\Factory;

use Doctrine\Common\Cache\Cache;
use Guzzle\Cache\DoctrineCacheAdapter;
use Guzzle\Plugin\Cache\CachePlugin;
use Guzzle\Plugin\Cache\DefaultCacheStorage;
use Psr\Log\LoggerInterface;

class DefaultClientFactory
{
    protected $clientClass;
    protected $host;
    protected $options;
    protected $defaults;
    protected $headers;
    protected $cache;

    public function __construct($clientClass, $host, $options, $defaults, $headers, Cache $cache = null, LoggerInterface $logger = null)
    {
        $this->clientClass = $clientClass;
        $this->host = $host;
        $this->options = $options;
        $this->defaults = $defaults;
        $this->headers = $headers;
        $this->cache = $cache;

        if ($logger !== null) {
            $this->options['logger'] = $logger;
        }
    }

    public function create()
    {
        $client = new $this->clientClass($this->host, $this->options);
        foreach ($this->defaults as $key => $value) {
            $client->setDefaultOption($key, $value);
        }
        $client->setDefaultHeaders($this->headers);

        if ($this->cache !== null) {
            $cachePlugin = new CachePlugin([
                'storage' => new DefaultCacheStorage(
                    new DoctrineCacheAdapter(
                        $this->cache
                    )
                ),
            ]);
            $client->addSubscriber($cachePlugin);
        }

        return $client;
    }
}
