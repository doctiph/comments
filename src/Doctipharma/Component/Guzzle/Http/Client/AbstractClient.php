<?php

namespace Doctipharma\Component\Guzzle\Http\Client;

use Guzzle\Http\Client;

abstract class AbstractClient extends Client
{
    protected $pathPrefix;
    protected $username;
    protected $password;

    public function setAuthentification($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function setPathPrefix($pathPrefix)
    {
        $this->pathPrefix = $pathPrefix;
    }

    public function createRequest($method = 'GET', $uri = null, $headers = null, $body = null, array $options = [])
    {
        if (null != $this->username && null != $this->password) {
            $this->setDefaultOption('auth', [$this->username, $this->password]);
        }

        return parent::createRequest($method, $this->pathPrefix . $uri, $headers, $body, $options);
    }

    public function getCustomUrl($url, $params)
    {
        return $params ? $url . '?' . http_build_query($params) : $url;
    }

    public function log($level, $message, $context = [])
    {
        $logger = $this->getConfig('logger');

        if ($logger && method_exists($logger, $level)) {
            $logger->$level($message, $context);
        }
    }
}
