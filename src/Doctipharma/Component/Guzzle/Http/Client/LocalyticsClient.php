<?php

namespace Doctipharma\Component\Guzzle\Http\Client;

class LocalyticsClient extends AbstractClient
{
    public function push($params = [])
    {
        return $this->post(sprintf('/v2/push/%s', $params['apiid']), null, json_encode($params['body']));
    }

    public function call($method, $params = [])
    {
        if (method_exists($this, $method)) {
            try {
                $request = call_user_func_array([$this, $method], $params);

                return $request->send()->json();
            } catch (\Exception $e) {
                $this->log('error', 'Localytics : ' . $e->getMessage());

                return [];
            }
        }

        $this->log('error', 'Localytics : method ' . $method . ' not found');

        return [];
    }
}
