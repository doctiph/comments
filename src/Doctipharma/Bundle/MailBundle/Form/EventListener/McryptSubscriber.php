<?php

namespace Doctipharma\Bundle\MailBundle\Form\EventListener;

use Doctipharma\Bundle\MailBundle\Entity\Comment;
use Doctipharma\Bundle\MailBundle\Entity\Thread;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class McryptSubscriber implements EventSubscriberInterface
{
    protected $mcrypt;
    protected $mcryptFields;

    public function __construct($mcrypt, $mcryptFields)
    {
        $this->mcrypt = $mcrypt;
        $this->mcryptFields = $mcryptFields;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'encryptField',
        ];
    }

    public function encryptField(FormEvent $event)
    {
        $data = $event->getData();

        switch ($object =$event->getForm()->getData()) {
            case $object instanceof Thread:
                $this->encrypt('thread', $data);

                break;

            case $object instanceof Comment:
                $this->encrypt('message', $data);

                break;
        }

        $event->setData($data);
    }

    public function encrypt($type, &$data)
    {
        if (isset($this->mcryptFields[$type])) {
            $fields = array_flip($this->mcryptFields[$type]);

            foreach ($data as $field => $value) {
                if (isset($fields[$field])) {
                    $data[$field] = $this->mcrypt->encrypt($value);
                }
            }
        }
    }
}
