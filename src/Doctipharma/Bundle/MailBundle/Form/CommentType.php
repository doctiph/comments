<?php

namespace Doctipharma\Bundle\MailBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentType extends AbstractType
{
    private $mcryptSubscriber;

    public function __construct($mcryptSubscriber)
    {
        $this->mcryptSubscriber = $mcryptSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('body', 'textarea');
        $builder->add('headers', 'textarea');
        $builder->add('cookie', 'hidden');

        // comment author
        $builder->add('username', 'text', ['required' => false]);
        $builder->add('email', 'text', ['required' => false]);
        $builder->add('userId', 'text', ['required' => false]);
        $builder->add('avatar', 'text', ['required' => false]);

        $builder->add('pharmacist_sender', 'checkbox', ['required' => false]);
        $builder->add('subject', 'text', ['required' => false]);
        $builder->add('shop_id', 'text', ['required' => false]);
        $builder->add('shop_email', 'text', ['required' => false]);
        $builder->add('shop_name', 'text', ['required' => false]);
        $builder->add('shop_contact', 'text', ['required' => false]);
        $builder->add('offer_id', 'text', ['required' => false]);
        $builder->add('order_id', 'text', ['required' => false]);

        $builder->add('file', 'file', ['required' => false]);

        if ($options['is_crypted'] == false) {
            $builder->addEventSubscriber($this->mcryptSubscriber);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => 'Doctipharma\Bundle\MailBundle\Entity\Comment',
            'csrf_protection' => false,
            'is_crypted'      => true,
        ]);
    }

    public function getName()
    {
        return 'doctipharma_mail_comment';
    }
}
