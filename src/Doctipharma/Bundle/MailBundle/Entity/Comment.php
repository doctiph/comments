<?php

namespace Doctipharma\Bundle\MailBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="doctimail__comment")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Comment
{
    const STATUS_UNREAD   = 0;
    const STATUS_READ     = 1;
    const STATUS_ARCHIVED = 2;

    protected $id;

    protected $thread;

    protected $userId;

    protected $username;

    protected $avatar;

    protected $email;

    protected $pharmacistSender;

    protected $subject;

    protected $body;

    protected $shopId;

    protected $shopEmail;

    protected $shopName;

    protected $shopContact;

    protected $offerId;

    protected $orderId;

    protected $status;

    protected $headers = '';

    protected $ip = '';

    protected $cookie;

    protected $createdAt;

    protected $filename;
    protected $file;
    protected $tempFile;
    protected $path;

    public function __construct()
    {
        $this->createdAt = $this->createdAt ? $this->createdAt : new DateTime();
        $this->path = $this->createdAt->format('Y/m/d');
    }

    /**
     * Return the comment unique id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param  string
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * @param Thread $thread
     */
    public function setThread(Thread $thread)
    {
        $this->thread = $thread;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isPharmacistSender()
    {
        return $this->pharmacistSender;
    }

    /**
     * @param  bool
     * @param mixed $value
     */
    public function setPharmacistSender($value)
    {
        $this->pharmacistSender = $value;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param  string
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param  string
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param  string
     * @param mixed $id
     */
    public function setShopId($id)
    {
        $this->shopId = $id;
    }

    /**
     * @return string
     */
    public function getShopEmail()
    {
        return $this->shopEmail;
    }

    /**
     * @param  string
     * @param mixed $email
     */
    public function setShopEmail($email)
    {
        $this->shopEmail = $email;
    }

    /**
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * @param  string
     * @param mixed $name
     */
    public function setShopName($name)
    {
        $this->shopName = $name;
    }

    /**
     * @return string
     */
    public function getShopContact()
    {
        return $this->shopContact;
    }

    /**
     * @param  string
     * @param mixed $name
     */
    public function setShopContact($name)
    {
        $this->shopContact = $name;
    }

    /**
     * @return string
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * @param  string
     * @param mixed $id
     */
    public function setOfferId($id)
    {
        $this->offerId = $id;
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param  string
     * @param mixed $id
     */
    public function setOrderId($id)
    {
        $this->orderId = $id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        $this->setIp();
    }

    /**
     * @param string $cookie
     */
    public function setCookie($cookie)
    {
        $this->cookie = $cookie;
    }

    /**
     * @return string
     */
    public function getCookie()
    {
        return $this->cookie;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * return $ip.
     */
    public function setIp()
    {
        if ($this->headers != '') {
            $headers = unserialize($this->headers);

            if (isset($headers['HTTP_TRUE_CLIENT_IP'])) {
                $this->ip = $headers['HTTP_TRUE_CLIENT_IP'];
            } elseif (isset($headers['HTTP_X_FORWARDED_FOR'])) {
                $this->ip = $headers['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($headers['REMOTE_ADDR'])) {
                $this->ip = $headers['REMOTE_ADDR'];
            }
        }

        return $this->ip;
    }

    public function setThreadLastCommentAt()
    {
        if ($this->thread) {
            $this->thread->setLastCommentAt($this->createdAt);
            $this->thread->setPharmacistSender($this->pharmacistSender);
            $this->thread->incrementNumComments();
        }
    }

    public function toArray()
    {
        return [
            'id'                   => $this->id,
            'thread'               => ($this->thread) ? $this->thread->toArray() : null,
            'userId'               => $this->userId,
            'username'             => $this->username,
            'avatar'               => $this->avatar,
            'email'                => $this->email,
            'is_pharmacist_sender' => $this->pharmacistSender,
            'subject'              => $this->subject,
            'body'                 => $this->body,
            'shop_id'              => $this->shopId,
            'shop_email'           => $this->shopEmail,
            'shop_name'            => $this->shopName,
            'shop_contact'         => $this->shopContact,
            'offer_id'             => $this->offerId,
            'order_id'             => $this->orderId,
            'status'               => $this->status,
            'headers'              => $this->headers,
            'ip'                   => $this->ip,
            'cookie'               => $this->cookie,
            'created_at'           => $this->createdAt,
        ];
    }

    public function isPrescription()
    {
        return strpos($this->thread->getId(), 'prescription') !== false;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    //==========================Upload=========================
    public function setFile($file)
    {
        $this->file = $file;
        if (isset($this->filename) && !empty($this->filename)) {
            $this->tempFile = $this->filename;
            $this->filename = null;
        } else {
            $this->filename = 'initial';
        }

        return $this;
    }

    public function getFile($format = null)
    {
        return $this->file;
    }

    public function getTempFile()
    {
        return $this->tempFile;
    }

    public function setTempFile($tempFile)
    {
        $this->tempFile = $tempFile;

        return $this;
    }

    public function getPath()
    {
        return $this->path ? $this->path : $this->createdAt->format('Y/m/d');
    }

    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    public function getAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->filename;
    }

    public function getUploadRootDir()
    {
        $path = realpath(__DIR__ . '/../../../../../data') . '/attachment/' . $this->getPath();

        if (!file_exists($path)) {
            mkdir($path, 0775, true);
        }

        return $path;
    }

    public function getAttachmentContent()
    {
        if ($this->filename && $this->filename != 'initial') {
            return file_get_contents($this->getAbsolutePath());
        }

        return;
    }

    public function setAttachmentContent($file)
    {
        $this->file = $file;

        return $this;
    }

    //==========================Upload=========================

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
