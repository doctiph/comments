<?php

namespace Doctipharma\Bundle\MailBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="doctimail__thread"
 * )
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Thread
{
    protected $id;

    protected $userId;

    protected $pharmacistSender;

    protected $permalink;

    protected $subject;

    protected $shopId;

    protected $shopEmail;

    protected $shopName;

    protected $shopContact;

    protected $offerId;

    protected $orderId;

    protected $numComments = 0;

    protected $createdAt;

    protected $lastCommentAt = null;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * Return the comment unique id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param  string
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return bool
     */
    public function isPharmacistSender()
    {
        return $this->pharmacistSender;
    }

    /**
     * @param  bool
     * @param mixed $value
     */
    public function setPharmacistSender($value)
    {
        $this->pharmacistSender = $value;
    }

    /**
     * @return string
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * @param  string
     * @param mixed $permalink
     */
    public function setPermalink($permalink)
    {
        $this->permalink = $permalink;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param  string
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param  string
     * @param mixed $id
     */
    public function setShopId($id)
    {
        $this->shopId = $id;
    }

    /**
     * @return string
     */
    public function getShopEmail()
    {
        return $this->shopEmail;
    }

    /**
     * @param  string
     * @param mixed $email
     */
    public function setShopEmail($email)
    {
        $this->shopEmail = $email;
    }

    /**
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * @param  string
     * @param mixed $name
     */
    public function setShopName($name)
    {
        $this->shopName = $name;
    }

    /**
     * @return string
     */
    public function getShopContact()
    {
        return $this->shopContact;
    }

    /**
     * @param  string
     * @param mixed $name
     */
    public function setShopContact($name)
    {
        $this->shopContact = $name;
    }

    /**
     * @return string
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * @param  string
     * @param mixed $id
     */
    public function setOfferId($id)
    {
        $this->offerId = $id;
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param  string
     * @param mixed $id
     */
    public function setOrderId($id)
    {
        $this->orderId = $id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets the creation date.
     *
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getLastCommentAt()
    {
        return $this->lastCommentAt;
    }

    /**
     * Sets the last comment date.
     *
     * @param DateTime $lastCommentAt
     */
    public function setLastCommentAt(DateTime $lastCommentAt)
    {
        $this->lastCommentAt = $lastCommentAt;
    }

    public function incrementNumComments()
    {
        ++$this->numComments;
    }

    public function toArray()
    {
        return [
            'id'                   => $this->id,
            'user_id'              => $this->userId,
            'is_pharmacist_sender' => $this->pharmacistSender,
            'permalink'            => $this->permalink,
            'subject'              => $this->subject,
            'shop_id'              => $this->shopId,
            'shop_email'           => $this->shopEmail,
            'shop_name'            => $this->shopName,
            'shop_contact'         => $this->shopContact,
            'offer_id'             => $this->offerId,
            'order_id'             => $this->orderId,
            'num_comments'         => $this->numComments,
            'created_at'           => $this->createdAt,
            'last_comment_at'      => $this->lastCommentAt,
        ];
    }
}
