<?php

namespace Doctipharma\Bundle\MailBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder
            ->root('doctipharma_mail')
                ->children()
                    ->arrayNode('service')->addDefaultsIfNotSet()
                        ->children()
                            ->arrayNode('manager')->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('thread')->cannotBeEmpty()->defaultValue('doctipharma_mail.manager.thread')->end()
                                ->end()
                            ->end()
                            ->arrayNode('form_factory')->addDefaultsIfNotSet()
                                ->children()
                                    ->scalarNode('comment')->cannotBeEmpty()->defaultValue('doctipharma_mail.form_factory.comment')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('client_factory')
                        ->prototype('array')
                            ->children()
                                ->scalarNode('class')->isRequired()->end()
                                ->scalarNode('host')->isRequired()->end()
                                ->scalarNode('cache')->isRequired()->end()
                                ->scalarNode('logger')->defaultNull()->end()
                                ->scalarNode('cache')->defaultNull()->end()
                                ->arrayNode('defaults')
                                    ->prototype('scalar')->end()
                                ->end()
                                ->arrayNode('options')
                                    ->prototype('scalar')->end()
                                ->end()
                                ->arrayNode('headers')
                                    ->prototype('scalar')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
