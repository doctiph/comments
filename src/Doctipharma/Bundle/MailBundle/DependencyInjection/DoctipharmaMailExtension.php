<?php

namespace Doctipharma\Bundle\MailBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class DoctipharmaMailExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);
        $config = $this->processConfiguration($configuration, $configs);
        $this->createClientFactories($container, $config['client_factory']);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');
        $loader->load('form.xml');
    }

    public function createClientFactories(ContainerBuilder $container, $factories)
    {
        foreach ($factories as $name => $factory) {
            foreach ($factory['headers'] as $key => $value) {
                unset($factory['headers'][$key]);
                $factory['headers'][str_replace('_', '', $key)] = $value;
            }

            $serviceId  = 'doctipharma.guzzle.client_factory.' . $name;

            $definition = new Definition('Doctipharma\Component\Guzzle\Http\Factory\DefaultClientFactory', [
                $factory['class'],
                $factory['host'],
                $factory['options'],
                $factory['defaults'],
                $factory['headers'],
                $factory['cache'] == null ? null : new Reference($factory['cache']),
                $factory['logger'] == null ? null : new Reference($factory['logger']),
            ]);

            $container->setDefinition($serviceId, $definition);
        }
    }
}
