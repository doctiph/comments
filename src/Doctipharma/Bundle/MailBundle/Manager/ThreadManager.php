<?php

namespace Doctipharma\Bundle\MailBundle\Manager;

use Doctipharma\Bundle\MailBundle\Entity\Comment;
use Doctipharma\Bundle\MailBundle\Entity\Thread;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ThreadManager
{
    protected $em;
    protected $dispatcher;
    protected $mcrypt;
    protected $mcryptFields;
    protected $threads;

    public function __construct(EventDispatcherInterface $dispatcher, EntityManager $em, $mcrypt, $mcryptFields)
    {
        $this->dispatcher = $dispatcher;
        $this->em = $em;
        $this->mcrypt = $mcrypt;
        $this->mcryptFields = $mcryptFields;
    }

    public function saveComment($comment)
    {
        $this->em->persist($comment);
        $this->em->persist($comment->getThread());
        $this->em->flush();
    }

    public function getStatusFromString($status)
    {
        if ($status == 'read') {
            return Comment::STATUS_READ;
        } elseif ($status == 'unread') {
            return Comment::STATUS_UNREAD;
        } elseif ($status == 'archived') {
            return Comment::STATUS_ARCHIVED;
        }

        return null;
    }

    /**
     * returns list of threads.
     *
     * @param unknown $customerId
     * @param unknown $sort
     * @param unknown $offset
     * @param unknown $limit
     */
    public function findUserThreads($customerId, $offset, $limit)
    {
        $repository = $this->em->getRepository('DoctipharmaMailBundle:Thread');

        $qb = $repository->createQueryBuilder('t')
            ->where('t.userId = :customerId')
            ->orderBy('t.lastCommentAt', 'DESC')
            ->setParameter('customerId', $customerId)
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $qb->getQuery()->execute();
    }

    /**
     * returns list of mail comments.
     *
     * @param unknown $searchParams
     * @param unknown $offset
     * @param unknown $limit
     */
    public function findMailComments($searchParams, $offset, $limit)
    {
        $repository = $this->em->getRepository('DoctipharmaMailBundle:Comment');

        $sortOrder = (isset($searchParams['sort_by_date'])) ? $searchParams['sort_by_date'] : 'DESC';

        $qb = $repository->createQueryBuilder('c')
            ->orderBy('c.createdAt', $sortOrder);

        if (isset($searchParams['shop_id'])) {
            $qb->andWhere('c.shopId = :shopId')
                ->setParameter('shopId', $searchParams['shop_id']);
        }

        if (isset($searchParams['id'])) {
            $qb->andWhere('c.userId = :userId')
            ->setParameter('userId', $searchParams['id']);
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        if (isset($searchParams['type'])) {
            if ($searchParams['type'] == 'order') {
                $qb->andWhere('c.orderId is not null');
            } elseif ($searchParams['type'] == 'offer') {
                $qb->andWhere('c.offerId is not null');
            } elseif ($searchParams['type'] == 'generic') {
                $qb->andWhere('c.orderId is null');
                $qb->andWhere('c.offerId is null');
            }
        }

        if (isset($searchParams['status'])) {
            if ($searchParams['status'] == 'read') {
                $qb->andWhere(sprintf('c.status = %d', Comment::STATUS_READ));
            } elseif ($searchParams['status'] == 'unread') {
                $qb->andWhere(sprintf('c.status = %d', Comment::STATUS_UNREAD));
            } elseif ($searchParams['status'] == 'archived') {
                $qb->andWhere(sprintf('c.status = %d', Comment::STATUS_ARCHIVED));
            }
        } else {
            $qb->andWhere(sprintf('c.status != %d', Comment::STATUS_ARCHIVED));
        }

        if (isset($searchParams['start']) && !empty($searchParams['start'])) {
            $qb
                ->andWhere('c.createdAt >= :start')
                ->setParameter('start', $searchParams['start'] . ' 00:00:00');
        }

        if (isset($searchParams['end']) && !empty($searchParams['end'])) {
            $qb
                ->andWhere('c.createdAt <= :end')
                ->setParameter('end', $searchParams['end'] . ' 59:59:59');
        }

        if (isset($searchParams['offer_id']) && $searchParams['offer_id']) {
            $qb
                ->andWhere('c.offerId = :offerId')
                ->setParameter('offerId', $searchParams['offer_id']);
        }

        if (isset($searchParams['order_id']) && $searchParams['order_id']) {
            $qb
                ->andWhere('c.orderId = :orderId')
                ->setParameter('orderId', $searchParams['order_id']);
        }

        if (isset($searchParams['order_id_like']) && $searchParams['order_id_like']) {
            $qb
                ->andWhere('c.orderId LIKE :orderId')
                ->setParameter('orderId', $searchParams['order_id_like']);
        }

        $scope = (isset($searchParams['scope'])) ? $searchParams['scope'] : null;

        if ($scope == 'received' || (isset($searchParams['from']) && $searchParams['from'] == 'client')) {
            $qb->andWhere('c.pharmacistSender = 0');
        } elseif ($scope == 'sent' || (isset($searchParams['from']) && $searchParams['from'] == 'shop')) {
            $qb->andWhere('c.pharmacistSender = 1');
        }

        if (isset($searchParams['only_count']) && $searchParams['only_count']) {
            return $qb->select('COUNT(c.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }

        return $qb->getQuery()->execute();
    }

    /**
     * returns list of threads.
     *
     * @param unknown $customerId
     * @param unknown $sort
     * @param unknown $offset
     * @param unknown $limit
     * @param mixed   $shopId
     * @param mixed   $scope
     * @param mixed   $searchParams
     */
    public function findShopThreads($shopId, $scope, $offset, $limit, $searchParams = [])
    {
        $searchParams['shop_id'] = $shopId;
        $searchParams['scope'] = $scope;

        return $this->findMailComments($searchParams, $offset, $limit);
    }

    /**
     * returns list of threads.
     *
     * @param unknown $customerId
     * @param unknown $sort
     * @param unknown $offset
     * @param unknown $limit
     * @param mixed   $threadId
     * @param mixed   $decrypt
     */
    public function findThreadComments($threadId, $decrypt = false)
    {
        $repository = $this->em->getRepository('DoctipharmaMailBundle:Comment');

        $comments = $repository->createQueryBuilder('c')
            ->select('c, t')
            ->leftJoin('c.thread', 't')
            ->where('c.thread = :threadId')
            ->orderBy('c.createdAt', 'ASC')
            ->setParameter('threadId', $threadId)
            ->getQuery()
            ->execute();

        if ($decrypt) {
            foreach ($comments as $key => $comment) {
                $comments[$key] = $this->decrypt($comment);
            }
        }

        return $comments;
    }

    public function getThread($threadId)
    {
        $repository = $this->em->getRepository('DoctipharmaMailBundle:Thread');

        return $repository->find($threadId);
    }

    public function getComment($commentId)
    {
        $repository = $this->em->getRepository('DoctipharmaMailBundle:Comment');

        return $repository->find($commentId);
    }

    /**
     * update status of mail comments.
     *
     * @param array $ids
     * @param int   $status
     *
     * @return bool
     */
    public function updateStatus($ids, $status)
    {
        $qb = $this->em->createQueryBuilder();

        $updatedRows = $qb->update('DoctipharmaMailBundle:Comment', 'c')
            ->set('c.status', $status)
            ->where($qb->expr()->in('c.id', $ids))
            ->getQuery()
            ->execute();

        return $updatedRows !== 0;
    }

    public function decrypt($comment)
    {
        if ($comment instanceof Comment) {
            foreach ($this->mcryptFields['message'] as $keyField => $field) {
                $this->decryptField($comment, $field);
            }

            $thread = $comment->getThread();
            if (!isset($this->threads[$thread->getId()])) {
                foreach ($this->mcryptFields['thread'] as $keyField => $field) {
                    $this->decryptField($thread, $field);
                }
                $comment->setThread($thread);
                $this->threads[$thread->getId()] = $thread;
            }
        }

        return $comment;
    }

    public function decryptField(&$obj, $field)
    {
        $setMethod = 'set' . ucfirst($field);
        $getMethod = 'get' . ucfirst($field);
        if (method_exists($obj, $setMethod) && method_exists($obj, $getMethod) && $obj->$getMethod()) {
            $decryptedValue = $this->mcrypt->decrypt($obj->$getMethod());
            $obj->$setMethod($decryptedValue);
        }
    }
}
