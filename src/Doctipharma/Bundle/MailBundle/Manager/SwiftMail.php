<?php

namespace Doctipharma\Bundle\MailBundle\Manager;

class SwiftMail extends \Swift_Message
{
    public $from;

    protected $letter_data = [];

    public function __construct($to)
    {
        parent::__construct($this->subject);

        $this->setTo($to);
    }

    public function getLetterData()
    {
        return $this->letter_data;
    }

    public function setLetterValue($key, $value)
    {
        $this->letter_data[$key] = $value;
    }

    public function setLetterData($letter_data)
    {
        $this->letter_data = $letter_data;

        return $this;
    }
}
