<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail;

use Doctipharma\Bundle\MailBundle\Manager\SwiftMail;

class NotificationMessageOfferToUser extends SwiftMail
{
    public $subject = 'Vous avez reçu un message à propos d\'une offre';
    protected $data = [];

    public function getLetterTemplate()
    {
        return 'Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\Letter\NotificationMessageOfferToUserLetter';
    }
}
