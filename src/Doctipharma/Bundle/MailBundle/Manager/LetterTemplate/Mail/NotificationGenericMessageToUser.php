<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail;

use Doctipharma\Bundle\MailBundle\Manager\SwiftMail;

class NotificationGenericMessageToUser extends SwiftMail
{
    public $subject = 'Vous avez reçu un message';
    protected $data = [];

    public function getLetterTemplate()
    {
        return 'Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\Letter\NotificationGenericMessageToUserLetter';
    }
}
