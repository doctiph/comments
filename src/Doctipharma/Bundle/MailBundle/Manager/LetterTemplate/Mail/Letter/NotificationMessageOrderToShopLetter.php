<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\Letter;

use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\LetterTemplate;

class NotificationMessageOrderToShopLetter extends LetterTemplate
{
    public function getDataKey()
    {
        return ['shop_name', 'message_subject', 'message_body', 'operator_name', 'operator_link', 'order_id'];
    }

    public function getTemplate()
    {
        return 'DoctipharmaMailBundle:Mail:message-on-order-to-shop.html.twig';
    }
}
