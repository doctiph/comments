<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\Letter;

use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\LetterTemplate;

class NotificationMessageOrderToUserLetter extends LetterTemplate
{
    public function getDataKey()
    {
        return ['shop_name', 'message_subject', 'message_body', 'operator_name', 'operator_link', 'order_id', 'customer_service_phone', 'guest_registration_link'];
    }

    public function getTemplate()
    {
        return 'DoctipharmaMailBundle:Mail:message-on-order-to-user.html.twig';
    }
}
