<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\Letter;

use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\LetterTemplate;

class NotificationGenericMessageToUserLetter extends LetterTemplate
{
    public function getDataKey()
    {
        return ['shop_name', 'message_subject', 'message_body', 'operator_name', 'operator_link', 'customer_service_phone'];
    }

    public function getTemplate()
    {
        return 'DoctipharmaMailBundle:Mail:message-generic-to-user.html.twig';
    }
}
