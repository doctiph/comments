<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\Letter;

use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\LetterTemplate;

class NotificationMessageOfferToUserLetter extends LetterTemplate
{
    public function getDataKey()
    {
        return ['shop_name', 'message_subject', 'message_body', 'operator_name', 'operator_link', 'customer_service_phone'];
    }

    public function getTemplate()
    {
        return 'DoctipharmaMailBundle:Mail:message-on-offer-to-user.html.twig';
    }
}
