<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail;

use Doctipharma\Bundle\MailBundle\Manager\SwiftMail;

class NotificationMessageOrderToShop extends SwiftMail
{
    public $subject = 'Vous avez reçu un message à propos d\'une commande';
    protected $data = [];

    public function getLetterTemplate()
    {
        return 'Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\Letter\NotificationMessageOrderToShopLetter';
    }
}
