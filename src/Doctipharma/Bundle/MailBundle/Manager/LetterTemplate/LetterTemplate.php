<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate;

abstract class LetterTemplate
{
    public function load($values)
    {
        $this->isValid($values);

        return $this->buildData($values);
    }

    protected function buildData($values)
    {
        $fields = [];

        foreach ($this->getDataKey() as $field) {
            $action = sprintf('get%s', ucfirst($field));

            if (method_exists($this, $action)) {
                $values[$field] = $this->$action($values[$field]);
            }

            $fields[$field] = $values[$field];
        }

        return $fields;
    }

    protected function isValid($values)
    {
        $mandatory = $this->getDataKey();

        if (count(array_diff($mandatory, array_keys($values)))) {
            $message = "Required fields are missing !\nExpected: %s";

            throw new \Exception(sprintf($message, implode(',', $mandatory)));
        }
    }
}
