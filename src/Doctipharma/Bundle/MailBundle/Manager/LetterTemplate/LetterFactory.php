<?php

namespace Doctipharma\Bundle\MailBundle\Manager\LetterTemplate;

class LetterFactory
{
    protected $templating;

    public function __construct(\Twig_Environment $templating)
    {
        $this->templating = $templating;
    }

    public function renderLetterBlock($letter_template_name, $block, $values)
    {
        $LetterTemplate = new $letter_template_name();

        $data = $LetterTemplate->load($values);

        return $this->templating->loadTemplate($LetterTemplate->getTemplate())->renderBlock($block, $data);
    }

    public function make($letter_template_name, $values)
    {
        $LetterTemplate = new $letter_template_name();

        $data = $LetterTemplate->load($values);

        return $this->templating->render($LetterTemplate->getTemplate(), $data);
    }
}
