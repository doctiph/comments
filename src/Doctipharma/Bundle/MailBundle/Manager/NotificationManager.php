<?php

namespace Doctipharma\Bundle\MailBundle\Manager;

use Doctipharma\Bundle\MailBundle\Entity\Comment;
use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\LetterFactory;
use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\NotificationGenericMessageToShop;
use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\NotificationGenericMessageToUser;
use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\NotificationMessageOfferToShop;
use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\NotificationMessageOfferToUser;
use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\NotificationMessageOrderToShop;
use Doctipharma\Bundle\MailBundle\Manager\LetterTemplate\Mail\NotificationMessageOrderToUser;
use Doctipharma\Component\Guzzle\Http\Client\LocalyticsClient;
use Encryption\Bundle\EncryptionBundle\Manager\EncryptorInterface;
use La\ExperianMailerBundle\Mailer\Mailer;
use La\MessagingBundle\Mailer\MailerSender;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class NotificationManager
{
    const PARAMETER_KEY_SUBJECT       = 'default_subject';
    const PARAMETER_KEY_FROM          = 'default_from';
    const PARAMETER_KEY_REPLY_TO      = 'default_reply_to';
    const PARAMETER_KEY_TEMPLATE_VARS = 'template_vars';

    protected $letter_factory;
    protected $mailer;
    protected $experianMailer;
    protected $localyticsClient;
    protected $encryptor;
    protected $logger;
    protected $encryptedFields;
    protected $isEncryptorActive;

    public function __construct(
        \Swift_Mailer $mailer,
        Mailer $experianMailer,
        LocalyticsClient $localyticsClient,
        LetterFactory $letter_factory,
        EncryptorInterface $encryptor,
        LoggerInterface $logger = null,
        $encryptedFields,
        $isEncryptorActive,
        array $parameters = []
    ) {
        $this->parameters = $parameters;
        $this->letter_factory = $letter_factory;
        $this->encryptor = $encryptor;
        $this->logger = $logger;
        $this->encryptedFields = $encryptedFields;
        $this->isEncryptorActive = $isEncryptorActive;
        $this->mailer = $mailer;
        $this->experianMailer = $experianMailer;
        $this->localyticsClient = $localyticsClient;
    }

    public function setFrom($from)
    {
        if (preg_match('/^(.*) <(.*)>$/', $from, $matches)) {
            $this->parameters[self::PARAMETER_KEY_FROM] = [$matches[2] => $matches[1]];
        } else {
            $this->parameters[self::PARAMETER_KEY_FROM] = $from;
        }
    }

    public function setSubject($subject)
    {
        $this->parameters[self::PARAMETER_KEY_SUBJECT] = $subject;
    }

    public function setReplyTo($replyTo)
    {
        $this->parameters[self::PARAMETER_KEY_REPLY_TO] = $replyTo;
    }

    public function getTemplateVarsValue($key)
    {
        return (isset($this->parameters[self::PARAMETER_KEY_TEMPLATE_VARS][$key])) ? $this->parameters[self::PARAMETER_KEY_TEMPLATE_VARS][$key] : '';
    }

    public function send(SwiftMail $mail)
    {
        $this->prepare($mail);

        $this->logger->info('Notification mail to send.', ['mail' => $mail]);

        $this->mailer->send($mail);
    }

    public function sendNotification(Comment $_comment)
    {
        try {
            $comment = $this->decryptComment($_comment);

            if ($mail = $this->getNotificationClassFromComment($comment)) {
                if ($_comment->isPrescription()) {
                    if ($comment['is_pharmacist_sender']) {
                        $this->sendPrescriptionNotification($comment);

                        $options = [
                            'email' => $comment['email'],
                            'meta' => [
                                'pharmacist' => $comment['shop_name'],
                            ],
                        ];

                        $this
                            ->experianMailer
                            ->sendMail('scan_ordo_client', $options);
                    } else {
                        $options = [
                            'email' => $comment['shop_email'],
                            'meta' => [
                                'ordo_id' => explode('_', $comment['thread']['id'])[1],
                            ],
                        ];

                        $this
                            ->experianMailer
                            ->sendMail('scan_ordo_pharmacist', $options);
                    }
                } else {
                    $mail->setLetterData([
                        'shop_name'              => $comment['shop_name'],
                        'message_subject'        => $comment['subject'],
                        'message_body'           => $comment['body'],
                        'operator_name'          => $this->getTemplateVarsValue('operator_name'),
                        'operator_link'          => $this->getTemplateVarsValue('operator_link'),
                        'customer_service_phone' => $this->getTemplateVarsValue('customer_service_phone'),
                    ]);

                    if (null != $comment['order_id']) {
                        $mail->setLetterValue('order_id', $comment['order_id']);
                        $guestRegistrationLink = $this->getTemplateVarsValue('operator_link') . $this->getTemplateVarsValue('guest_registration_url');
                        $url = str_replace('{crm_id}', $comment['userId'], $guestRegistrationLink) . '?' . http_build_query([
                            'orderId' => $comment['order_id'],
                            'email' => $comment['email'],
                        ]);
                        $mail->setLetterValue('guest_registration_link', $url);
                    }

                    $this->setFrom($this->parameters[self::PARAMETER_KEY_FROM]);
                    $this->send($mail);
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('Error during sending of notification mail.', ['exception' => $e]);
        }
    }

    public function sendPrescriptionNotification($comment)
    {
        foreach (['ios', 'android'] as $os) {
            $subject = explode('_', $comment['thread']['id']);

            $message = [
                'campaign_key' => 'prescription',
                'target_type' => 'customer_id',
                'messages' => [[
                    'target' => $comment['userId'],
                    'alert' => $comment['body'],
                    $os => ['extra' => ['url' => $this->parameters[$os . '_url'] . '/' . $subject[1]]],
                ]],
            ];

            $response = $this->localyticsClient->call('push', [['apiid' => $this->parameters[$os . '_id'], 'body' => $message]]);
            $this->logger->info('Localytics response : ', $response);
        }
    }

    protected function getBodyText(SwiftMail $mail)
    {
        return $this->letter_factory->renderLetterBlock($mail->getLetterTemplate(), 'body_text', $mail->getLetterData());
    }

    protected function getBodyHtml(SwiftMail $mail)
    {
        return $this->letter_factory->renderLetterBlock($mail->getLetterTemplate(), 'body_html', $mail->getLetterData());
    }

    protected function prepare(SwiftMail $mail)
    {
        $from = is_null($mail->from) ? $this->parameters[self::PARAMETER_KEY_FROM] : $mail->from;
        $subject = is_null($mail->subject) ? $this->parameters[self::PARAMETER_KEY_SUBJECT] : $mail->subject;

        $mail->setSubject($subject);
        $mail->setFrom($from);
        $mail->setReplyTo($this->parameters[self::PARAMETER_KEY_REPLY_TO]);

        $mail->setBody($this->getBodyHtml($mail), 'text/html');

        if ($bodyText = $this->getBodyText($mail)) {
            $mail->addPart($bodyText, 'text/plain');
        }
    }

    protected function getNotificationClassFromComment(array $comment)
    {
        $email = ($comment['is_pharmacist_sender']) ? $comment['email'] : $comment['shop_email'];

        if (empty($email)) {
            return null;
        }

        if (null != $comment['order_id']) {
            return ($comment['is_pharmacist_sender']) ? new NotificationMessageOrderToUser($email) : new NotificationMessageOrderToShop($email);
        } elseif (null != $comment['offer_id']) {
            return ($comment['is_pharmacist_sender']) ? new NotificationMessageOfferToUser($email) : new NotificationMessageOfferToShop($email);
        } else {
            return ($comment['is_pharmacist_sender']) ? new NotificationGenericMessageToUser($email) : new NotificationGenericMessageToShop($email);
        }
    }

    protected function decryptComment(Comment $comment)
    {
        $result = $comment->toArray();

        if (!$this->isEncryptorActive) {
            return $result;
        }

        if (isset($this->encryptedFields['message'])) {
            foreach ($this->encryptedFields['message'] as $field) {
                if (isset($result[$field]) && !empty($result[$field])) {
                    $result[$field] = $this->encryptor->decrypt($result[$field]);
                }
            }
        }

        if (isset($result['thread']) && (null != $result['thread']) && isset($this->encryptedFields['thread'])) {
            foreach ($this->encryptedFields['thread'] as $field) {
                if (isset($result['thread'][$field]) && !empty($result['thread'][$field])) {
                    $result['thread'][$field] = $this->encryptor->decrypt($result['thread'][$field]);
                }
            }
        }

        return $result;
    }
}
