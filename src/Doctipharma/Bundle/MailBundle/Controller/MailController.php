<?php

namespace Doctipharma\Bundle\MailBundle\Controller;

use Doctipharma\Bundle\MailBundle\Entity\Comment;
use Doctipharma\Bundle\MailBundle\Entity\Thread;
use FOS\RestBundle\View\View;
use La\CommentBundle\Controller\ThreadController;
use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
use Symfony\Component\HttpFoundation\Request;

class MailController extends ThreadController
{
    public function listAction(Request $request)
    {
        $scope  = $request->query->get('scope');
        $offset = $request->query->get('offset');
        $limit  = $request->query->get('limit');

        if ($shopId = $request->query->get('shop_id')) {
            $searchParams = $request->query->all();

            $comments = $this->get('doctipharma_mail.manager.thread')->findShopThreads($shopId, $scope, $offset, $limit, $searchParams);
        } else {
            if ($request->query->get('only_count', false)) {
                $searchParams = $request->query->all();

                $comments = $this->get('doctipharma_mail.manager.thread')->findMailComments($searchParams, $offset, $limit);
            } else {
                $customerId = $request->query->get('id');

                $comments = $this->get('doctipharma_mail.manager.thread')->findUserThreads($customerId, $offset, $limit);
            }
        }

        $view = View::create()->setData(['messages' => $comments]);

        $view->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comments'));

        return $this->container->get('fos_rest.view_handler')->handle($view);
    }

    public function getMessageAction(Request $request)
    {
        $id = $request->query->get('id');

        $comment = $this->get('doctipharma_mail.manager.thread')->getComment($id);

        $view = View::create()->setData(['result' => $comment]);

        $view->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comments'));

        return $this->container->get('fos_rest.view_handler')->handle($view);
    }

    public function threadListAction(Request $request)
    {
        $threadId = $request->query->get('id');
        $decrypted = $request->query->get('decrypted', false);
        $comments = $this->get('doctipharma_mail.manager.thread')->findThreadComments($threadId, $decrypted);
        $view = View::create()->setData(['messages' => $comments]);
        $view->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comments'));

        return $this->container->get('fos_rest.view_handler')->handle($view);
    }

    public function answerAction(Request $request, $id)
    {
        $this->checkSecurity();

        if (!$comment = $this->get('doctipharma_mail.manager.thread')->getComment($id)) {
            throw $this->createNotFoundException(sprintf('Message with id %s not found', $id));
        }

        if (!$thread = $this->get('doctipharma_mail.manager.thread')->getThread($comment->getThread()->getId())) {
            throw $this->createNotFoundException(sprintf('Thread with id %s not found', $comment->getThread()->getId()));
        }

        $answer = new Comment();
        $answer->setThread($thread);
        $answer->setStatus(Comment::STATUS_UNREAD);
        $form = $this->createForm('doctipharma_mail_comment', $answer, [
            'is_crypted' => $request->query->get('crypted', true),
        ]);
        if (!$form->submit($request)->isValid()) {
            throw new \Exception('not valid' . $form->getErrorsAsString());
        }

        $this->get('doctipharma_mail.manager.thread')->saveComment($answer);
        $this->sendNotification($answer);

        return $this->container->get('fos_rest.view_handler')->handle(View::create()->setData([
            'success' => true,
            'comment' => $comment,
            'answer' => $answer,
        ]));
    }

    public function newMailAction(Request $request)
    {
        $this->checkSecurity();

        $thread = new Thread();
        $form = $this->createForm('doctipharma_mail_thread', $thread, [
            'is_crypted' => $request->query->get('crypted', true),
        ]);
        if (!$form->submit($request)->isValid()) {
            throw new \Exception('not valid' . $form->getErrorsAsString());
        }

        $comment = new Comment();
        $comment->setThread($thread);
        $comment->setStatus(Comment::STATUS_UNREAD);
        $form = $this->createForm('doctipharma_mail_comment', $comment, [
            'is_crypted' => $request->query->get('crypted', true),
        ]);
        if (!$form->submit($request)->isValid()) {
            throw new \Exception('not valid' . $form->getErrorsAsString());
        }

        try {
            $this->get('doctipharma_mail.manager.thread')->saveComment($comment);
            $this->sendNotification($comment);

            return $this->container->get('fos_rest.view_handler')->handle(View::create()->setData([
                'comment' => $comment,
            ]));
        } catch (\Exception $e) {
            return $this->container->get('fos_rest.view_handler')->handle(View::create()->setData([
                'error' => $e->getMessage(),
            ]));
        }
    }

    /**
     * Update status of one or more messages (read/unread/archived).
     *
     * @param Request $request
     */
    public function updateStatusAction(Request $request)
    {
        if (!$status = $request->query->get('status')) {
            throw $this->createNotFoundException('Status not provided');
        }

        if (!$ids = $request->query->get('ids')) {
            throw $this->createNotFoundException('id(s) not provided');
        }

        if (($status = $this->get('doctipharma_mail.manager.thread')->getStatusFromString($status)) === null) {
            throw $this->createNotFoundException('Invalid status');
        }

        $result = $this->get('doctipharma_mail.manager.thread')->updateStatus($ids, $status);

        $view = View::create()->setData(['success' => $result]);

        return $this->container->get('fos_rest.view_handler')->handle($view);
    }

    protected function sendNotification(Comment $comment)
    {
        $this->get('doctipharma_mail.manager.notification')->sendNotification($comment);
    }
}
