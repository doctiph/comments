<?php

namespace La\Lib\Experian;

/**
 * Class Token
 * @package La\Lib\Experian
 */
class Token
{
    const EXPERIAN_API_TOKEN_URL = 'https://api.ccmp.eu/services/authorization/oauth2/token';

    /** @var string */
    private $clientId;

    /** @var string */
    private $consumerKey;

    /** @var string */
    private $consumerSecret;

    /** @var string */
    private $token = null;

    private $client = null;

    /**
     * @var null
     */
    private static $instance = null;

    /**
     * @param $clientId
     * @param $consumerKey
     * @param $consumerSecret
     * @return Token|null
     */
    public static function getInstance($clientId, $consumerKey, $consumerSecret)
    {

        if (is_null(self::$instance)) {
            self::$instance = new Token($clientId, $consumerKey, $consumerSecret);
        }

        return self::$instance;
    }


    /**
     * Token constructor.
     * @param $clientId
     * @param $consumerKey
     * @param $consumerSecret
     */
    private function __construct($clientId, $consumerKey, $consumerSecret)
    {
        $this->clientId = $clientId;
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;

        $this->client = new \Guzzle\Http\Client();
    }

    /**
     * @return string
     */
    public function getToken()
    {
        if (is_null($this->token)) {
            $this->generateToken();
        }

        return $this->token;
    }

    /**
     *
     */
    private function generateToken()
    {
        $postFields = array(
            "grant_type" => "password",
            "client_id" => $this->clientId,
            "username" => $this->consumerKey,
            "password" => $this->consumerSecret,
        );

        $request = $this->client->createRequest('POST', static::EXPERIAN_API_TOKEN_URL, [], $postFields);
        $res = $request->send();

        if (200 !== $res->getStatusCode()) {
            throw new \Exception("Error unable to generate token ");
        }

        $data = $res->json();

        if (is_null($data)) {
            throw new \Exception("Error unable to generate token ");
        }

        $this->token = $data['access_token'];
    }
}
