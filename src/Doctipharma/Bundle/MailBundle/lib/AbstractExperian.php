<?php

namespace La\Lib\Experian;

use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class AbstractExperian
 * @package La\Lib\Experian
 */
abstract class AbstractExperian
{
    const EXPERIAN_API_EMAIL_CAMPAIGN_URL = "https://api.ccmp.eu/services2/api/EmailCampaign";
    const EXPERIAN_API_CONTENT_BLOCK_URL = "https://api.ccmp.eu/services2/api/ContentBlock";
    const EXPERIAN_API_FOLDER_ACCOUNT_URL = "https://api.ccmp.eu/services/api/Folder/account";
    const EXPERIAN_API_FOLDER_URL = "https://api.ccmp.eu/services/api/Folder";
    const EXPERIAN_API_TABLE_URL = "https://api.ccmp.eu/services2/api/Table";

    /**
     * @var Token|null
     */
    protected $token;

    protected $client = null;

    /**
     * AbstractExperian constructor.
     * @param $clientId
     * @param $consumerKey
     * @param $consumerSecret
     * @param LoggerInterface|null $logger
     */
    public function __construct($clientId, $consumerKey, $consumerSecret, LoggerInterface $logger = null)
    {
        $this->token = Token::getInstance($clientId, $consumerKey, $consumerSecret);

        $this->client = new \Guzzle\Http\Client();

        $this->logger = $logger ?: new NullLogger();
    }

    /**
     * @param $method
     * @param $url
     * @param array $postFields
     * @param array $headers
     * @return array
     * @throws \Exception
     */
    protected function send($method, $url, $postFields = [], $headers = [])
    {
        try {
            $headers = array_merge(
                [
                    'Accept' => 'application/json',
                    'Content-Type' => ' application/json',
                    'Authorization' => "Bearer ".$this->token->getToken(),
                ],
                $headers
            );


            $request = $this->client->createRequest($method, $url, $headers, $postFields);
            $response = $request->send();
            $data = $response->json();

            if (is_null($data)) {
                throw new \Exception("Error unable to get data");
            }
            $res = [
                'code' => $response->getStatusCode(),
                'md5' => md5(json_encode($data)),
                'result' => $data,
            ];

            return $res;

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            throw new \Exception("Error during the call: ".$e->getMessage());
        }
    }

    use LoggerAwareTrait;
}
