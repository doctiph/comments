<?php

namespace Doctipharma\Bundle\MailBundle\EventListener;

use Doctipharma\Bundle\MailBundle\Entity\Comment;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CommentSubscriber implements EventSubscriber
{
    protected $mcryptManager;

    public function __construct($mcryptManager)
    {
        $this->mcryptManager = $mcryptManager;
    }

    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
            'postPersist',
            'postUpdate',
            'postLoad',
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->preUpload($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->preUpload($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->upload($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->upload($args);
    }

    public function preUpload(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Comment) {
            if ($entity->getFile() instanceof UploadedFile) {
                $entity->setFilename(sha1(uniqid(mt_rand(), true)) . '.' . $entity->getFile()->guessClientExtension());
            }
        }
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Comment) {
            $source = $entity->getAttachmentContent();
            $entity->setAttachmentContent(base64_encode($this->mcryptManager->decrypt($source)));
        }
    }

    public function upload(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Comment) {
            $file = $entity->getFile();
            if (null === $file || !($file instanceof UploadedFile)) {
                return;
            }

            $file->move($entity->getUploadRootDir(), $entity->getFilename());

            $source = file_get_contents($entity->getAbsolutePath());
            $encryptedSource = $this->mcryptManager->encrypt($source);
            file_put_contents($entity->getAbsolutePath(), $encryptedSource);
        }
    }
}
