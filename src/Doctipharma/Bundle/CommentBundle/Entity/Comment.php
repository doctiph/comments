<?php

namespace Doctipharma\Bundle\CommentBundle\Entity;

use La\CommentBundle\Model\Comment as AbstractComment;
use La\CommentBundle\Validation\Constraints\RatingConstraint;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class Comment extends AbstractComment
{
    /**
     * All ancestors of the comment.
     *
     * @var string
     */
    protected $ancestors = '';

    /**
     * avis verifies external product id.
     *
     * @var string
     */
    protected $externalProductId = '';

    /**
     * avis verifies external order id.
     *
     * @var string
     */
    protected $externalOrderId = '';

    /**
     * om order commercial id.
     *
     * @var int
     */
    protected $orderCommercialId = '';

    /**
     * @return array
     */
    public function getAncestors()
    {
        return $this->ancestors ? explode('/', $this->ancestors) : [];
    }

    /**
     * @param  array
     */
    public function setAncestors(array $ancestors)
    {
        $this->ancestors = implode('/', $ancestors);
        $this->depth = count($ancestors);
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new RatingConstraint());
    }

    /**
     * Gets the avis verifies external product id.
     *
     * @return string
     */
    public function getExternalProductId()
    {
        return $this->externalProductId;
    }

    /**
     * Sets the avis verifies external product id.
     *
     * @param string $externalProductId the external product id
     *
     * @return self
     */
    public function setExternalProductId($externalProductId)
    {
        $this->externalProductId = $externalProductId;

        return $this;
    }

    /**
     * Gets the avis verifies external order id.
     *
     * @return string
     */
    public function getExternalOrderId()
    {
        return $this->externalOrderId;
    }

    /**
     * Sets the avis verifies external order id.
     *
     * @param string $externalOrderId the external order id
     *
     * @return self
     */
    public function setExternalOrderId($externalOrderId)
    {
        $this->externalOrderId = $externalOrderId;

        return $this;
    }

    /**
     * Gets the om order commercial id.
     *
     * @return int
     */
    public function getOrderCommercialId()
    {
        return $this->orderCommercialId;
    }

    /**
     * Sets the om order commercial id.
     *
     * @param int $orderCommercialId the order commercial id
     *
     * @return self
     */
    public function setOrderCommercialId($orderCommercialId)
    {
        $this->orderCommercialId = $orderCommercialId;

        return $this;
    }
}
