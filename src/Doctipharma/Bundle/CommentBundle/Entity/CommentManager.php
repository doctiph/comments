<?php

namespace Doctipharma\Bundle\CommentBundle\Entity;

use La\CommentBundle\Entity\CommentManager as BaseCommentManager;
use La\CommentBundle\Model\ModerationManager;

/**
 * Doctharma ORM CommentManager.
 */
class CommentManager extends BaseCommentManager
{
    public function findCommentByExternalIds($threadId, $externalOrderId, $externalProductId = null)
    {
        $criteria = [
            'thread' => $threadId,
            'externalOrderId' => $externalOrderId,
        ];

        if ($externalProductId) {
            $criteria['externalProductId'] = $externalProductId;
        }

        return $this->repository->findOneBy($criteria);
    }

    public function removeByExternalIds($threadId, $externalOrderId, $externalProductId = null, $by = '')
    {
        $qb = $this->repository->createQueryBuilder('c');
        $res = $qb->update()
            ->set('c.moderationState', ModerationManager::MODERATION_NOT_OK)
            ->set('c.moderatedBy', $qb->expr()->literal($by))
            ->set('c.visibility', 0)
            ->where('c.thread = :threadId')
            ->andWhere('c.externalOrderId = :externalOrderId')
            ->setParameter('threadId', $threadId)
            ->setParameter('externalOrderId', $externalOrderId)
            ->getQuery()
            ->execute();

        if ($externalProductId) {
            $qb->andWhere('c.externalProductId = :externalProductId')
                ->setParameter('externalProductId', $externalProductId);
        }

        $res = $qb->getQuery()
            ->execute();

        return $res;
    }

    public function emClear()
    {
        $this->em->clear();
    }
}
