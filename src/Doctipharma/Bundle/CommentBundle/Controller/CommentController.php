<?php

namespace Doctipharma\Bundle\CommentBundle\Controller;

use FOS\RestBundle\View\View;
use La\CommentBundle\Controller\ThreadController;
use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use La\CommentBundle\Entity\Thread;

class CommentController extends ThreadController
{
    public function getCommentUserAction($thread_id, $user_id)
    {
        $threadManager = $this->container->get('la_comment.manager.thread');
        $commentManager = $this->container->get('la_comment.manager.comment');

        $thread = $threadManager->findThreadById($thread_id);
        $commentIds = $commentManager->hasUserPosted($thread, $user_id);

        if (!$commentIds) {
            throw new NotFoundHttpException(sprintf("No comment found for user id '%s' and for thread with id '%s'", $user_id, $thread_id));
        }

        $comment = $commentManager->findCommentById($commentIds[0]);

        if (null === $thread || null === $comment || $comment->getThread() !== $thread) {
            throw new NotFoundHttpException(sprintf("No comment found for user id '%s' and for thread with id '%s'", $user_id, $thread_id));
        }

        $view = View::create()
            ->setData(['comment' => $comment, 'thread' => $thread])
            ->setTemplate(new TemplateReference('LaCommentBundle', 'Thread', 'comment'));

        $this->addModerationCache($view);

        return $this->getViewHandler()->handle($view);
    }


    public function getDistributionOfRatingAction($thread_id)
    {
        $repository = $this->getDoctrine()->getRepository('DoctipharmaCommentBundle:Comment');

        $qb = $repository->createQueryBuilder('c')
            ->select('c.rating', 'COUNT(c.id) AS total')
            ->where('c.thread = :threadId')
            ->groupBy('c.rating')
            ->setParameter('threadId', $thread_id)
        ;

        $queryResult = $qb->getQuery()->getResult();
        $result = [];
        foreach ($queryResult as $data) {
            $result[$data['rating']] = $data;
        }

        for ($i = 1 ; $i <= 5 ; $i++) {
           if (!array_key_exists($i, $result)) {
               $result[$i] = ['rating' => $i , 'total' => 0];
           }
        }

        ksort($result);

        return $result;
    }

    /**
     * @return \FOS\RestBundle\View\ViewHandler
     */
    private function getViewHandler()
    {
        return $this->container->get('fos_rest.view_handler');
    }
}
