<?php

namespace Doctipharma\Bundle\CommentBundle\EventListener;

use La\AdminBundle\Event\AdminNavEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DoctipharmaAdminListener implements EventSubscriberInterface
{
    const ADMIN_NAV_EVENT_PRIORITY = 1;

    protected $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return [
            'admin.nav.links' => ['onAdminNavLinksRequest', static::ADMIN_NAV_EVENT_PRIORITY],
        ];
    }

    public function onAdminNavLinksRequest(AdminNavEvent $event)
    {
        $event->addLinks(
            [
                'name' => 'Se déconnecter',
                'icon' => 'sign-out',
                'links' => [
                    [
                        'name' => 'Se déconnecter',
                        'url' => $this->router->generate('logout'),
                        'role' => 'ROLE_LA_ADMIN',
                    ],
                ],
            ]
        );
    }
}
