<?php

namespace Doctipharma\Bundle\CommentBundle\Command;

use Doctipharma\Bundle\CommentBundle\Entity\Comment;
use La\CommentBundle\Entity\Thread;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportReviewFromAvisVerifiesCommand extends ContainerAwareCommand
{
    const MODERATED_BY = 'avis_verifies';
    //(all_|)reviews => New version retrieve review
    //(all_|)reviews => Old version but with type product/pharmacy
    const FILE_MATCHING = '/^((all_|)reviews)|avis/';
    const MIN_NB_COLUMN = 10;
    const FILE_MAX_LINE_LENGTH = 10000;
    const FILE_DELIMITER = "\t";

    const CSV_PURGE_FILENAME  = 'product-purge-%s.csv';
    const DELIMITER = ';';

    private $filename;

    private $filePath;

    private $fileMapping = [];

    private $logInfo = [];

    private $purgeFile;
    private $purgeFilename;
    private $purgeCount;

    private $csvHeader = [
        'action'             => 'action',
        'idVente'            => 'id. Vente',
        'idProduit'          => 'id. Produit',
        'refVente'           => 'ref. vente',
        'refProduit'         => 'ref. produit',
        'horodateAvis'       => 'horodate avis',
        'avis'               => 'avis',
        'note'               => 'note',
        'nom'                => 'nom',
        'prenom'             => 'prenom',
        'email'              => 'email',
        'type'               => 'type',
        'idPharmacyRelated'  => 'id_pharmacy_related',
        'nbEchange'          => 'nb Echange',
        'echangeHorodate'    => 'horodate',
        'echangeOrigine'     => 'origine',
        'echangeCommentaire' => 'commentaire',
    ];

    protected function configure()
    {
        $this
            ->setName('doctipharma:avis-verifies:import')
            ->setDescription('Import review from avis verifies command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->fileHeader = [];
            $this->purgeCount = 0;
            $this->logInfo = [
                'time_start' => time(true),
                'new'        => 0,
                'updated'    => 0,
                'deleted'    => 0,
                'error'      => 0,
            ];
            $date = new \DateTime();
            $output->writeln(sprintf('<info>==== Start Import review %s  ====</info>', $date->format('d/m/Y H:i:s')));

            $this->commentManager = $this->getContainer()->get('la_comment.manager.comment');
            $this->threadManager = $this->getContainer()->get('la_comment.manager.thread');

            $this->initToPurgeFile();
            $reviewDirPath = $this->getContainer()->getParameter('avis_verifies')['product_review'];
            $this->executeReviewWithDir($reviewDirPath, $output);
            $this->copyPurgeFile();

            $reviewDirPath = $this->getContainer()->getParameter('avis_verifies')['site_review'];
            $this->executeReviewWithDir($reviewDirPath, $output, true);

            $output->writeln('<info>Import review : New ' . $this->logInfo['new'] . '</info>');
            $output->writeln('<info>Import review : Updated ' . $this->logInfo['updated'] . '</info>');
            $output->writeln('<info>Import review : Deleted ' . $this->logInfo['deleted'] . '</info>');
            $output->writeln('<info>Import review : Process Time ' . (time(true) - $this->logInfo['time_start']) . 's</info>');
        } catch (\Exception $e) {
            $output->writeln('<error>Import review : Error</error>');
            $output->writeln('<error>Import review : ' . $e->getMessage() . '</error>');
            $this->sendErrorMail($e->getMessage());
        }
        $output->writeln('<info>==== End Import review  ====</info>');
    }

    protected function executeReviewWithDir($pathConfig, $output, $isSiteReview = false)
    {
        $dirPath   = $pathConfig['folder'];
        $backupDir = $pathConfig['backup'];

        if (count(array_diff(scandir($dirPath), ['..', '.'])) < 2 && $dh = opendir($dirPath)) {
            while (($filename = readdir($dh)) !== false) {
                if (!preg_match(self::FILE_MATCHING, $filename)) {
                    continue;
                }
                $this->filename = $filename;
                $this->filePath = $dirPath . '/' . $this->filename;
                $file = file($this->filePath);

                 // create backup directory
                if (!file_exists($backupDir)) {
                    mkdir($backupDir, 0775, true);
                }

                if (!empty($file)) {
                    $this->parseFileReview($file, $output, $isSiteReview);
                }

                //Backup file
                rename($this->filePath, $backupDir . '/' . $this->filename);
            }
            closedir($dh);
        } else {
            $output->writeln('<error>Import review : No reviews folder or multiple files in folder</error>');
            $this->sendErrorMail('No reviews folder or multiple files in folder');
        }
    }

    protected function parseFileReview($file, $output, $isSiteReview = false)
    {
        foreach ($file as $line => $content) {
            $data = explode(self::FILE_DELIMITER, $content);
            if ($line === 0) {
                array_splice($data, count($this->csvHeader));
                $this->fileHeader = array_flip($data);
                continue;
            }

            if (count($data) < self::MIN_NB_COLUMN) {
                continue;
            }

            //Get or create thread
            $threadId = $isSiteReview ? 'site' : $this->getValueFromData($data, 'type') . '_' . $this->getValueFromData($data, 'refProduit');
            if (!$thread = $this->threadManager->findThreadById(['id' => $threadId])) {
                $thread = $this->threadManager->createThread($threadId);
                //TODO Avis Verifies new version retrieve review possibility to add product_url
                $baseUrl = $this->getContainer()->getParameter('doctipharma_domain');
                $thread->setPermalink($baseUrl . 'p/' . $this->getValueFromData($data, 'refProduit') . '-redirect');
                $thread->setType($thread::THREAD_TYPE);
                $this->threadManager->saveThread($thread);
            }

            switch ($this->getValueFromData($data, 'action')) {
                case 'NEW':
                case 'UPDATE':
                    $comment = $this->commentManager->findCommentByExternalIds(
                        $threadId,
                        $this->getValueFromData($data, 'idVente'),
                        $this->getValueFromData($data, 'idProduit')
                    );
                    if (!$comment) {
                        ++$this->logInfo['new'];
                        $comment = $this->commentManager->createComment($thread);
                    } else {
                        ++$this->logInfo['updated'];
                        if ($thread->getRating() > 0) {
                            $thread->removeRating($comment->getRating());
                        }
                    }
                    $this->fillAndSaveComment($data, $comment, $isSiteReview);
                    $thread->addRating($comment->getRating());

                    $this->addProductToPurge($this->getValueFromData($data, 'refProduit'));

                    break;

                case 'DELETE':
                    $this->commentManager->removeByExternalIds(
                        $threadId,
                        $this->getValueFromData($data, 'idVente'),
                        $this->getValueFromData($data, 'idProduit'),
                        self::MODERATED_BY
                    );
                    if ($thread->getRating() > 0) {
                        $thread->removeRating($this->getValueFromData($data, 'note'));
                    }
                    ++$this->logInfo['deleted'];
                    break;

                default:
                    $this->logInfo['error']++;
                    $output->writeln('<warn> Action not catch ' . $this->getValueFromData($data, 'action') . '</warn>');
                    break;
            }
            $this->threadManager->saveThread($thread);
            $this->commentManager->emClear();
        }
    }

    protected function getValueFromData(array $data, $key, $offset = 0)
    {
        if (array_key_exists($this->csvHeader[$key], $this->fileHeader)) {
            return $data[$this->fileHeader[$this->csvHeader[$key]] + $offset];
        } else {
            return;
        }
    }

    protected function fillAndSaveComment($data, $comment, $isSiteReview = false)
    {
        if ($comment) {
            !$isSiteReview && $comment->setExternalProductId($this->getValueFromData($data, 'idProduit'));
            $comment->setCreatedAt(new \DateTime(substr($this->getValueFromData($data, 'horodateAvis'), 0, -5)));
            $comment->setExternalOrderId($this->getValueFromData($data, 'idVente'));
            $comment->setBody($this->getValueFromData($data, 'avis'));
            $comment->setRating($this->getValueFromData($data, 'note'));
            $comment->setEmail($this->getValueFromData($data, 'email'));
            $comment->setUsername($this->formatUsername($data));
            $comment->setOrderCommercialId($this->getValueFromData($data, 'refVente'));
            $this->commentManager->saveComment($comment);
        }

        // if ($this->getValueFromData($data, 'nbEchange') > 0) {
        //     //Delete all replies for this comment ?
        //     $nbEchange = $this->getValueFromData($data, 'nbEchange');
        //     for ($i=0; $i < $nbEchange; $i++) {
        //         $subComment = $commentManager->createComment($thread, $comment);
        //         $subComment->setCreatedAt(new \DateTime($this->getValueFromData($data, 'echangeHorodate')));
        //         $subComment->setBody($this->getValueFromData($data, 'echangeCommentaire'));
        //         // $subComment->setExternalProductId($this->getValueFromData($data, 'idProduit'));
        //         // $subComment->setExternalOrderId($this->getValueFromData($data, 'idVente'));
        //         $subComment->setUsername($this->getValueFromData($data, 'echangeOrigine', ($i*3)));
        //         $commentManager->saveComment($subComment);
        //     }
        // }
    }

    protected function formatUsername($data)
    {
        return sprintf('%s %s.', $this->getValueFromData($data, 'prenom'), substr($this->getValueFromData($data, 'nom'), 0, 1));
    }

    protected function sendErrorMail($body)
    {
        $mailer = $this->getContainer()->get('swiftmailer.mailer.default');
        $message = new \Swift_Message();
        $message->setSubject('[Comments] Error import review avis verifies')
            ->setBody($body)
            ->setTo($this->getContainer()->getParameter('mailer_sender_support'))
            ->setSender($this->getContainer()->getParameter('mailer_sender_support'));
        $mailer->send($message);
    }

    protected function addProductToPurge($productId)
    {
        fputcsv($this->purgeFile, ['product', $productId], self::DELIMITER);
        ++$this->purgeCount;
    }

    protected function initToPurgeFile()
    {
        $date = new \DateTime();
        $this->purgeFilename = sprintf(self::CSV_PURGE_FILENAME, $date->format('Ymd_His'));
        $this->purgeFile = fopen('/tmp/' . $this->purgeFilename, 'w');
        fputcsv($this->purgeFile, ['type', 'id'], self::DELIMITER);
    }

    protected function copyPurgeFile()
    {
        if ($this->purgeCount > 0) {
            $filepath = stream_get_meta_data($this->purgeFile)['uri'];
            $destPath = $this->getContainer()->getParameter('csv_purge_file_dest');
            fclose($this->purgeFile);
            rename($filepath, $destPath . $this->purgeFilename);
        }
    }
}
