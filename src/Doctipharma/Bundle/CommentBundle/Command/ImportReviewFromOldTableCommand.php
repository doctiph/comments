<?php

namespace Doctipharma\Bundle\CommentBundle\Command;

use Doctipharma\Bundle\CommentBundle\Entity\Comment;
use La\CommentBundle\Entity\Thread;
use La\CommentBundle\Model\CommentInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportReviewFromOldTableCommand extends ContainerAwareCommand
{
    const MODERATED_BY = 'avis_verifies';
    const FILE_PREFIX = 'avis';
    const MIN_NB_COLUMN = 10;
    const FILE_MAX_LINE_LENGTH = 10000;
    const FILE_DELIMITER = "\t";

    protected $filename;

    protected $filePath;

    protected $fileMapping = [];

    protected $logInfo = [];

    protected $logger;

    private $csvHeader = [
        'action'             => 'action',
        'idVente'            => 'id. Vente',
        'idProduit'          => 'id. Produit',
        'refVente'           => 'ref. vente',
        'refProduit'         => 'ref. produit',
        'horodateAvis'       => 'horodate avis',
        'avis'               => 'avis',
        'note'               => 'note',
        'nom'                => 'nom',
        'prenom'             => 'prenom',
        'email'              => 'email',
        'type'               => 'type',
        'idPharmacyRelated'  => 'id_pharmacy_related',
        'nbEchange'          => 'nb Echange',
        'echangeHorodate'    => 'horodate',
        'echangeOrigine'     => 'origine',
        'echangeCommentaire' => 'commentaire',
    ];

    protected function configure()
    {
        $this
            ->setName('doctipharma:old-review:import')
            ->setDescription('Import review from old table')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logInfo = [
            'time_start' => time(true),
            'new'        => 0,
            'updated'    => 0,
            'deleted'    => 0,
            'error'      => 0,
        ];
        $this->commentManager = $this->getContainer()->get('la_comment.manager.comment');
        $this->threadManager = $this->getContainer()->get('la_comment.manager.thread');

        $checkComments = $this->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository('DoctipharmaCommentBundle:Comment')
            ->findOneBy(['externalOrderId' => 'old_comment']);

        if (count($checkComments) === 1) {
            $output->writeln('<info>Import old comments already executed</info>');

            return true;
        }

        $oldComments = $this->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository('LaCommentBundle:Comment')
            ->findBy(['visibility' => 1, 'moderationState' => CommentInterface::STATE_VALIDATED]);

        $this->addOldComments($oldComments, $output);

        $output->writeln('<info>Import review : New ' . $this->logInfo['new'] . '</info>');
        $output->writeln('<info>Import review : Process Time ' . (time(true) - $this->logInfo['time_start']) . 's</info>');
    }

    protected function addOldComments(array $oldComments, $output)
    {
        foreach ($oldComments as $oldComment) {
            //Get thread
            $thread = $this->threadManager->findThreadById(['id' => $oldComment->getThread()->getId()]);
            $thread->setType($thread::THREAD_TYPE);
            $comment = $this->commentManager->createComment($thread);
            $comment->setCreatedAt($oldComment->getCreatedAt());
            $comment->setExternalOrderId('old_comment');
            $comment->setExternalProductId('old_comment');
            $comment->setBody($oldComment->getTitle() . "\n" . $oldComment->getBody());
            $comment->setRating($oldComment->getRating());
            $comment->setEmail($oldComment->getEmail());
            $comment->setUsername($oldComment->getUsername());

            $thread->addRating($comment->getRating());

            $this->commentManager->saveComment($comment);

            ++$this->logInfo['new'];
            $this->commentManager->emClear();
        }
    }
}
